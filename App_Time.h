#ifndef _APP_TIME_H_
#define _APP_TIME_H_
 
#include "main.h"
 
void App_TimeInit(void);
void App_TimeHandle(void);
 
#endif //_APP_TIME_H_
