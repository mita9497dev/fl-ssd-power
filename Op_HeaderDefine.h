#ifndef _OP_HEADER_DEFINE_H_
#define _OP_HEADER_DEFINE_H_
 
#define DEBUG(value) do { Serial.printf("[%d:%d:%d] ", g_aubTime[HOUR_INDEX], g_aubTime[MINUTE_INDEX], g_aubTime[SECOND_INDEX]); Serial.println(value);} while(0); 
#define C_ON (1)
#define C_OFF (0)
#define sbi(str,bit) (str |= (1<<bit)) // set bit in register
#define cbi(str,bit) (str &= ~(1<<bit)) // clear bit in register
#define tbi(str,bit) (str ^= (1<<bit)) // toggle bit in register

/*--- Define for Thingsboard ---*/
#ifdef MITA_DEV 
#define THINGSBOARD_TOKEN   "bcugjOWBmaQCmwoTgvvr" // OS SITE 4
#define THINGSBOARD_SERVER  "thingsboard.cloud"
#define THINGSBOARD_PORT    8883U
#define WIFI_SSID           "tepbac.com"
#define WIFI_PASS           "unicorn2025"
#else 
#define THINGSBOARD_TOKEN   "bcugjOWBmaQCmwoTgvvr"
#define THINGSBOARD_SERVER  "thingsboard.cloud"
#define THINGSBOARD_PORT    8883U
#endif

/*--- Define for time ---*/
#define TIME_20MS_BY_1MS    (20)
#define TIME_100MS_BY_1MS   (100)
#define TIME_1S_BY_100MS    (10)
#define TIME_60S_BY_100MS   (600)
/*--- Define for wifi ---*/
#define ssidPath    "/ssid.txt"
#define passPath    "/pass.txt"
#define ipPath      "/ip.txt"
#define gatewayPath "/gateway.txt"
#define BlynkTokenPath   "/blynk.txt"
 
#define PARAM_INPUT_1 "ssid"
#define PARAM_INPUT_2 "pass"
#define PARAM_INPUT_3 "ip"
#define PARAM_INPUT_4 "gateway"
#define PARAM_INPUT_5 "blynk"
/*--- Define for Button ---*/
#define BUTTON_1  (36)
#define BUTTON_2  (9)
#define BUTTON_3  (10)
/*--- Define for Key ---*/
#define KEY_1CLICK_COUNT (3)
#define KEY_0_5SEC_COUNT (25)
#define KEY_1SEC_COUNT (50)
#define KEY_1_5SEC_COUNT (75)
#define KEY_2SEC_COUNT (100)
#define KEY_3SEC_COUNT (150)
#define KEY_5SEC_COUNT (250)
#define KEY_7SEC_COUNT (350)
#define KEY_SHORTERROR_COUNT (1500)
 
#define KEY_OFF_DEBOUCE (1)
#define KEY_ON_DEBOUNCE (2)
 
/*--- Define for output ---*/
#define POWER_1 (14)
#define POWER_2 (12)
#define LED_OUTPUT_1  (33)
#define LED_OUTPUT_2  (32)
#define LED_STATUS_1  (2)
#define LED_STATUS_2  (13)
/*--- Define for time ---*/
#define SECOND_INDEX  (0)
#define MINUTE_INDEX  (1)
#define HOUR_INDEX    (2)
#define DATE_INDEX    (3)
#define MONTH_INDEX   (4)
#define YEAR_INDEX    (5)
/*--- Define for Ethernet ---*/
#define DEBUG_ETHERNET_WEBSERVER_PORT       Serial
#define _ETHERNET_WEBSERVER_LOGLEVEL_       3
#ifdef ETH_CLK_MODE
#undef ETH_CLK_MODE
#endif

#ifndef MITA_DEV
#define ETH_CLK_MODE    ETH_CLOCK_GPIO17_OUT
#endif
 
#define ETH_POWER_PIN   -1                  // Pin# of the enable signal for the external crystal oscillator (-1 to disable for internal APLL source)
#ifndef MITA_DEV
#define ETH_TYPE        ETH_PHY_LAN8720     // Type of the Ethernet PHY (LAN8720 or TLK110)
#endif
#define ETH_ADDR        1                   // I²C-address of Ethernet PHY (0 or 1 for LAN8720, 31 for TLK110)
#define ETH_MDC_PIN     23                  // Pin# of the I²C clock signal for the Ethernet PHY
#define ETH_MDIO_PIN    18                  // Pin# of the I²C IO signal for the Ethernet PHY
#define ETH_CONNECT_PIN 32
#define RESET_PIN       13
#define TRIG_5V_PIN     2
#define TRIG_12V_PIN    15
#endif // _OP_HEADER_DEFINE_H_
