#ifndef _MAIN_H_
#define _MAIN_H_
 
#include <Arduino.h>
#include <TimeLib.h>
 
#include "stdio.h"
#include "stdint.h"
#include "stdbool.h"
#include "String.h"
 
#include "Op_HeaderVariable.h"
#include "Op_HeaderDefine.h"
#include "Op_HeaderFlag.h"
 
#endif // _MAIN_H_
