#include "App_Time.h"
 
void App_TimeInit(void)
{
  setTime(1676966541);
    
  ALARM_POWE_1_ENABLE_FLAG = LOW;
  ALARM_POWE_2_ENABLE_FLAG = LOW;
}
void App_TimeHandle(void)
{
  g_aubTime[DATE_INDEX] = day();
  g_aubTime[MONTH_INDEX] = month();
  g_aubTime[YEAR_INDEX] = year();
  g_aubTime[HOUR_INDEX] = hour();
  g_aubTime[MINUTE_INDEX] = minute();
  g_aubTime[SECOND_INDEX] = second();
  /*--- Alarm for power 1 ---*/
  if(ALARM_POWE_1_ENABLE_FLAG == HIGH)
  {
    if(g_aubTime[SECOND_INDEX] == g_aubAlarm1OnTime[SECOND_INDEX] &&
        g_aubTime[MINUTE_INDEX] == g_aubAlarm1OnTime[MINUTE_INDEX] &&
        g_aubTime[HOUR_INDEX] == g_aubAlarm1OnTime[HOUR_INDEX])
    {
        g_ubPower1State = HIGH;
        POWER_1_CHANGE_FLAG = C_ON;
    }
 
    if(g_aubTime[SECOND_INDEX] == g_aubAlarm1OffTime[SECOND_INDEX] &&
    g_aubTime[MINUTE_INDEX] == g_aubAlarm1OffTime[MINUTE_INDEX] &&
    g_aubTime[HOUR_INDEX] == g_aubAlarm1OffTime[HOUR_INDEX])
    {
        g_ubPower1State = LOW;
        POWER_1_CHANGE_FLAG = C_ON;
    }
  }
  /*--- Time off 1 ---*/
  if(POWER_1_RESTART_HIGH_FLAG == C_ON)
  {
    POWER_1_CHANGE_FLAG = C_ON;
    g_ubPower1State = C_ON;
    POWER_1_RESTART_HIGH_FLAG = C_OFF;
  }
  if(TB_CONNECT_DONE_FLAG == C_ON)
  {
    if(TB_IS_CONNECT_FLAG == C_OFF)
    {
      if(TIME_OFF_1_ENABLE_FLAG == C_ON && POWER_1_RESTART_DONE_FLAG == C_OFF)
      {
        if(g_ubPower1State == C_ON)
        {
          if(g_ulSetTimeOff1 > 0)
          {
            DEBUG("Time of count: ");
            DEBUG(g_ulSetTimeOff1);
            g_ulSetTimeOff1--;
          }
          else
          {
            POWER_1_CHANGE_FLAG = C_ON;
            g_ubPower1State = C_OFF;
            POWER_1_RESTART_HIGH_FLAG =  C_ON;
            POWER_1_RESTART_DONE_FLAG = C_ON;
          }
        }
      }
    }
  }
}
