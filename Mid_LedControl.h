#ifndef _MID_LED_CONTROL_H_
#define _MID_LED_CONTROL_H_
 
#include "main.h"
 
void Mid_LedInit(void);
void Mid_LedAllOn(void);
void Mid_LedAllOff(void);
void Mid_LedHanler(void);
 
#endif // _MID_LED_CONTROL_H_
