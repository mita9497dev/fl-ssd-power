
#define GLOBAL_DEFINE (1)
#define MITA_DEV

#include "main.h"

#ifdef MITA_DEV
#include <WiFi.h>
#else
#include <WebServer_WT32_ETH01.h>
#endif
#include <WiFiClientSecure.h>

constexpr char ROOT_CERT[] PROGMEM = R"(-----BEGIN CERTIFICATE-----
MIIFazCCA1OgAwIBAgIRAIIQz7DSQONZRGPgu2OCiwAwDQYJKoZIhvcNAQELBQAw
TzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2Vh
cmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwHhcNMTUwNjA0MTEwNDM4
WhcNMzUwNjA0MTEwNDM4WjBPMQswCQYDVQQGEwJVUzEpMCcGA1UEChMgSW50ZXJu
ZXQgU2VjdXJpdHkgUmVzZWFyY2ggR3JvdXAxFTATBgNVBAMTDElTUkcgUm9vdCBY
MTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAK3oJHP0FDfzm54rVygc
h77ct984kIxuPOZXoHj3dcKi/vVqbvYATyjb3miGbESTtrFj/RQSa78f0uoxmyF+
0TM8ukj13Xnfs7j/EvEhmkvBioZxaUpmZmyPfjxwv60pIgbz5MDmgK7iS4+3mX6U
A5/TR5d8mUgjU+g4rk8Kb4Mu0UlXjIB0ttov0DiNewNwIRt18jA8+o+u3dpjq+sW
T8KOEUt+zwvo/7V3LvSye0rgTBIlDHCNAymg4VMk7BPZ7hm/ELNKjD+Jo2FR3qyH
B5T0Y3HsLuJvW5iB4YlcNHlsdu87kGJ55tukmi8mxdAQ4Q7e2RCOFvu396j3x+UC
B5iPNgiV5+I3lg02dZ77DnKxHZu8A/lJBdiB3QW0KtZB6awBdpUKD9jf1b0SHzUv
KBds0pjBqAlkd25HN7rOrFleaJ1/ctaJxQZBKT5ZPt0m9STJEadao0xAH0ahmbWn
OlFuhjuefXKnEgV4We0+UXgVCwOPjdAvBbI+e0ocS3MFEvzG6uBQE3xDk3SzynTn
jh8BCNAw1FtxNrQHusEwMFxIt4I7mKZ9YIqioymCzLq9gwQbooMDQaHWBfEbwrbw
qHyGO0aoSCqI3Haadr8faqU9GY/rOPNk3sgrDQoo//fb4hVC1CLQJ13hef4Y53CI
rU7m2Ys6xt0nUW7/vGT1M0NPAgMBAAGjQjBAMA4GA1UdDwEB/wQEAwIBBjAPBgNV
HRMBAf8EBTADAQH/MB0GA1UdDgQWBBR5tFnme7bl5AFzgAiIyBpY9umbbjANBgkq
hkiG9w0BAQsFAAOCAgEAVR9YqbyyqFDQDLHYGmkgJykIrGF1XIpu+ILlaS/V9lZL
ubhzEFnTIZd+50xx+7LSYK05qAvqFyFWhfFQDlnrzuBZ6brJFe+GnY+EgPbk6ZGQ
3BebYhtF8GaV0nxvwuo77x/Py9auJ/GpsMiu/X1+mvoiBOv/2X/qkSsisRcOj/KK
NFtY2PwByVS5uCbMiogziUwthDyC3+6WVwW6LLv3xLfHTjuCvjHIInNzktHCgKQ5
ORAzI4JMPJ+GslWYHb4phowim57iaztXOoJwTdwJx4nLCgdNbOhdjsnvzqvHu7Ur
TkXWStAmzOVyyghqpZXjFaH3pO3JLF+l+/+sKAIuvtd7u+Nxe5AW0wdeRlN8NwdC
jNPElpzVmbUq4JUagEiuTDkHzsxHpFKVK7q4+63SM1N95R1NbdWhscdCb+ZAJzVc
oyi3B43njTOQ5yOf+1CceWxG1bQVs5ZufpsMljq4Ui0/1lvh+wjChP4kqKOJ2qxq
4RgqsahDYVvTH9w7jXbyLeiNdd8XM2w9U/t7y0Ff/9yi0GE44Za4rF2LN9d11TPA
mRGunUHBcnWEvgJBQl9nJEiU0Zsnvgc/ubhPgXRR4Xq37Z0j4r7g1SgEEzwxA57d
emyPxgcYxn/eR44/KJ4EBs+lVDR3veyJm+kXQ99b21/+jh5Xos1AnX5iItreGCc=
-----END CERTIFICATE-----
)";

#include <ThingsBoard.h>
#include <HTTPClient.h>
#include <ESP32Ping.h>
#include <Preferences.h>
Preferences preferences;

#include <WiFiManager.h>
#include <WiFi.h>
#include "SPIFFS.h"
#include <AsyncTCP.h>
#include <WiFiClient.h>

#include <SimpleTimer.h>
#include <ESPAsyncWebServer.h>
#include "App_Control.h"
#include "App_Time.h"
#include "Mid_FileOperation.h"
#include "Mid_KeyControl.h"
#include "Mid_ControlPower.h"
#include "Mid_LedControl.h"

SimpleTimer timer;
AsyncWebServer server(80);

//Change the virtual pins according the rooms
#define VPIN_BUTTON_1 V0
#define VPIN_BUTTON_2 V1
#define VPIN_TIME_OFF_1 V2
#define VPIN_TERMINAL V9
#define VPIN_TIMER1INPUT V10
#define VPIN_ALARM_1 V3
#define VPIN_ALARM_2 V4

/*****************
 ** Thingsboard **
 *****************/
WiFiClientSecure ethClient;
constexpr uint32_t MAX_MESSAGE_SIZE PROGMEM = 128U;
ThingsBoardSized<MAX_MESSAGE_SIZE> tb(ethClient);

constexpr std::array<const char*, 1U> REQUESTED_SHARED_ATTRIBUTES = {
  "network_timeout"
};

void processSharedAttributeRequest(const Shared_Attribute_Data &data) {
  for (auto it = data.begin(); it != data.end(); ++it) {
    Serial.println(it->key().c_str());
    Serial.println(it->value().as<const char*>());

    if (it->key() == "network_timeout") {
      NetworkTimeoutReset = it->value().as<int>();
    }
  }
}

const Attribute_Request_Callback sharedAttributesRequestCallback(REQUESTED_SHARED_ATTRIBUTES.cbegin(), REQUESTED_SHARED_ATTRIBUTES.cend(), &processSharedAttributeRequest);
const Shared_Attribute_Callback  sharedAttributesSubscirbeCallback(REQUESTED_SHARED_ATTRIBUTES.cbegin(), REQUESTED_SHARED_ATTRIBUTES.cend(), &processSharedAttributeRequest);

RPC_Response processCommand(const RPC_Data &data) {
  // Process data
  // data is the payload
  String command = data["cmd"];

  Serial.print(F("Command: "));
  Serial.println(command);

  if (command == "reboot_outlet1") {
    POWER_1_REBOOT_FLAG = C_ON;
  } else if (command == "reboot_outlet2") {
    POWER_2_REBOOT_FLAG = C_ON;
  } else if (command == "ping") {
    PING_PROCESS_FLAG = C_ON;
  }

  // Just an response example
  return RPC_Response(NULL, 1);
}

RPC_Response setValue1(const RPC_Data &data);
RPC_Response setValue2(const RPC_Data &data);
RPC_Response setValue3(const RPC_Data &data);
RPC_Response getValue1(const RPC_Data &data);
RPC_Response getValue2(const RPC_Data &data);
RPC_Response getValue3(const RPC_Data &data);

const std::array<RPC_Callback, 7U> callbacks = {
  // command is rpc method map with processCommand function
  RPC_Callback{ "command", processCommand },
  RPC_Callback{ "setValue1", setValue1 },  // Is relay 1
  RPC_Callback{ "setValue2", setValue2 },  // is relay 2
  RPC_Callback{ "setValue3", setValue3 },  // is reboot timeout
  RPC_Callback{ "getValue1", getValue1 },
  RPC_Callback{ "getValue2", getValue2 },
  RPC_Callback{ "getValue3", getValue3 }
};

/*--- For Network ---*/
bool App_CheckModuleNetwork(void);
void App_ModuleNetworkReconnect(void);
void App_GetExternalIp(void);
/*--- For Ethernet ---*/
void App_EthernetInit(void);
/*--- For Wifi ---*/
bool App_WifiStationInit(void);
void App_WifiAPInit(void);
void App_WifiCheckConnect(void);
/*--- For Thingsboard ---*/
void App_ThingsboardCheckConnect(void);
void App_ThingsboardConnect(void);
void App_ThingsboardSync(void);
void App_StateSync(void);
/*--- For Operation ---*/
void ProcessMain(void);
void Timer20msCallBack(void);
void Timer100msCallBack(void);
void Timer5000msCallback(void);
void PingTask(void);

void setup() {
  Serial.begin(115200);
  preferences.begin("mainv1", false);
  ethClient.setCACert(ROOT_CERT);

  App_TimeInit();
  initSPIFFS();
  Mid_KeyInit();
  Mid_ControlPowerInit();
  Mid_LedInit();

  digitalWrite(LED_STATUS_1, HIGH);
  pinMode(TRIG_5V_PIN, OUTPUT);
  pinMode(TRIG_12V_PIN, OUTPUT);
  pinMode(RESET_PIN, OUTPUT);
  pinMode(ETH_CONNECT_PIN, OUTPUT);
  digitalWrite(TRIG_5V_PIN, LOW);
  digitalWrite(TRIG_12V_PIN, LOW);
  digitalWrite(RESET_PIN, LOW);
  digitalWrite(ETH_CONNECT_PIN, LOW);

  timer.setInterval(TIME_20MS_BY_1MS, Timer20msCallBack);
  timer.setInterval(TIME_100MS_BY_1MS, Timer100msCallBack);
  timer.setInterval(TIME_100MS_BY_1MS, Timer5000msCallback);

  // Init status
  g_tLedStatus = TB_DISCONNECTED_STAGE;
  g_ubPower1State = 1;
  g_ubPower2State = 1;
  POWER_1_CHANGE_FLAG = C_ON;
  POWER_2_CHANGE_FLAG = C_ON;
  POWER_1_REBOOT_FLAG = C_OFF;
  POWER_2_REBOOT_FLAG = C_OFF;
  STATE_SYNC_FLAG = C_ON;
  g_ubPreWifistatus = NULL;
  g_ubPreThingsboardStatus = true;
  g_ulNetworkLastTimeReconnect = 0;
  g_dUpTime = 0;
  NetworkTimeoutReset = preferences.getUInt("nw_timeout", 50);
  deviceIp = "";

  #ifdef MITA_DEV
  App_WifiStationInit();
  #else
  App_EthernetInit();
  #endif
}

void loop() {
  ProcessMain();
  timer.run();
}

void ProcessMain(void) {
  if (TB_1S_CHECK_CONNECT_FLAG == C_ON) {
    App_ThingsboardCheckConnect();
    TB_1S_CHECK_CONNECT_FLAG = C_OFF;

    if (TB_IS_CONNECT_FLAG == C_OFF) {
      App_ThingsboardConnect();
    }
  }

  /*--- App control ---*/
  App_Controlhandle();
  /*--- Mid Power ---*/
  Mid_ControlPowerHandle();

  /*--- Thingsboard ---*/
  if (WIFI_IS_CONNECT_FLAG == C_ON) {
    tb.loop();
    delay(100);

    /*-- Ping --*/
    if (PING_PROCESS_FLAG == C_ON) {
      PingTask();
      PING_PROCESS_FLAG = C_OFF;
    }

    if (g_ubThingsboardStatus && PING_SEND_DATA_FLAG == C_ON) {
      DEBUG("PING_SEND_DATA_FLAG");
      tb.sendTelemetryFloat("PacketLoss", g_ufPacketsLoss);
      tb.sendTelemetryFloat("Latency", g_ufLatency);
      PING_SEND_DATA_FLAG = C_OFF;
    }

    if (WIFI_GET_EXTERNAL_IP == C_ON) {
      App_GetExternalIp();
      if (deviceIp.length() > 0) {
        tb.sendAttributeString("ip_address", deviceIp.c_str());
      }

      WIFI_GET_EXTERNAL_IP = C_OFF;
    }

  } else if (millis() - g_ulNetworkLastTimeReconnect > 30000L) {
    App_ModuleNetworkReconnect();
    g_ulNetworkLastTimeReconnect = millis();
  }

  if (
    (WIFI_IS_CONNECT_FLAG == C_OFF || !g_ubThingsboardStatus) && 
    (WIFI_LAST_STATUS_FLAG == C_ON) && 
    (millis() - g_ulNetworkLastTimeDisconnect > NetworkTimeoutReset * 1000)
  ) {
    // ESP.restart();
    POWER_1_REBOOT_FLAG = C_ON;
    WIFI_LAST_STATUS_FLAG = C_OFF;
    g_ulNetworkLastTimeDisconnect = millis();
    return;
  }

  /*--- Wifi Check connect ---*/
  if (WIFI_1S_CHECK_CONNECT_FLAG == C_ON) {
    App_CheckModuleNetwork();
    WIFI_1S_CHECK_CONNECT_FLAG = C_OFF;
  }

  /*--- Time 1S count ---*/
  if (TIME_1S_COUNT_FLAG == C_ON) {
    App_TimeHandle();
    TIME_1S_COUNT_FLAG = C_OFF;
  }

  /*--- Thingsboard sync ---*/
  if (TB_SYNC_FLAG == C_ON) {
    App_ThingsboardSync();
    TB_SYNC_FLAG = C_OFF;
  }

  if (STATE_SYNC_FLAG == C_ON) {
    App_StateSync();
    STATE_SYNC_FLAG = C_OFF;
  }
}

void Timer5000msCallback(void) {
  static uint16_t ub5sCount = 0;
  if (ub5sCount < TIME_60S_BY_100MS) {
    ub5sCount++;
  } else {
    ub5sCount = 0;
    g_dUpTime += (double)0.0138;
    STATE_SYNC_FLAG = C_ON;
    PING_PROCESS_FLAG = C_ON;
  }
}

void Timer20msCallBack(void) {
  /*--- Key ---*/
  Mid_KeyHandler();
  Mid_LedHanler();
}

void Timer100msCallBack(void) {
  static uint8_t ub1sCount = 0;
  if (ub1sCount < TIME_1S_BY_100MS) {
    ub1sCount++;
  } else {
    ub1sCount = 0;
    TB_1S_CHECK_CONNECT_FLAG = C_ON;
    WIFI_1S_CHECK_CONNECT_FLAG = C_ON;
    TIME_1S_COUNT_FLAG = C_ON;
  }
}

void Mid_syncPower1(void) {
  tb.sendAttributeInt("outlet1", g_ubPower1State);
}

void Mid_syncPower2(void) {
  tb.sendAttributeInt("outlet2", g_ubPower2State);
}

void App_StateSync(void) {
  tb.sendAttributeFloat("uptime", g_dUpTime);
}

RPC_Response setValue1(const RPC_Data &data) {
  g_ubPower1State = data;
  POWER_1_CHANGE_FLAG = C_ON;
  return RPC_Response(NULL, g_ubPower1State);
}

RPC_Response setValue2(const RPC_Data &data) {
  g_ubPower2State = data;
  POWER_2_CHANGE_FLAG = C_ON;
  return RPC_Response(NULL, g_ubPower2State);
}

RPC_Response setValue3(const RPC_Data &data) {
  int value = data;
  if (value < 0) {
    DEBUG(F("Invalid value"));
  }

  NetworkTimeoutReset = data;
  preferences.putUInt("nw_timeout", NetworkTimeoutReset);
  return RPC_Response(NULL, NetworkTimeoutReset);
}

RPC_Response getValue1(const RPC_Data &data) {
  return RPC_Response(NULL, g_ubPower1State);
}

RPC_Response getValue2(const RPC_Data &data) {
  return RPC_Response(NULL, g_ubPower2State);
}

RPC_Response getValue3(const RPC_Data &data) {
  return RPC_Response(NULL, NetworkTimeoutReset);
}

/*-- Ping time --*/
void PingTask(void) {
  DEBUG(F("Ping..."));

  // IPAddress ip(45, 77, 34, 83);
  IPAddress ip(142, 250, 66, 142);
  bool ret = Ping.ping(ip, 3);

  if (ret) {
    g_ufPacketsLoss = Ping.getPacketsLoss();
    g_ufLatency = Ping.averageTime();
  } else {
    g_ufPacketsLoss = 0;
    g_ufLatency = 0;
  }

  DEBUG(g_ufPacketsLoss);
  DEBUG(g_ufLatency);

  PING_SEND_DATA_FLAG = C_ON;
}

/*--- For Network ---*/
bool App_CheckModuleNetwork(void) {
#ifdef MITA_DEV
  g_ubWifistatus = WiFi.status();
  if (g_ubPreWifistatus != g_ubWifistatus) {
    switch (g_ubWifistatus) {
      case 0:
        break;
      case 1:
        break;
      case 2:
        break;
      case 3:
        WIFI_IS_CONNECT_FLAG = C_ON;
        WIFI_LAST_STATUS_FLAG = C_ON;
        WIFI_GET_EXTERNAL_IP = C_ON;
        DEBUG("WIFI: Connected");
        break;
      case 4:
        WIFI_IS_CONNECT_FLAG = C_OFF;
        DEBUG("WIFI: Connect fails");
        break;
      case 5:
        WIFI_IS_CONNECT_FLAG = C_OFF;
        DEBUG("WIFI: Connection is lost");
        break;
      case 6:
        WIFI_IS_CONNECT_FLAG = C_OFF;
        DEBUG("WIFI: Disconnect from a network");
        // WIFI_ENTER_ST_MODE = C_ON;
        break;
      default:
        break;
    }

    g_ubPreWifistatus = g_ubWifistatus;
  }
  return g_ubWifistatus == 3;
#else
  g_ubWifistatus = WT32_ETH01_isConnected();
  if (g_ubPreWifistatus != g_ubWifistatus) {
    WIFI_IS_CONNECT_FLAG = g_ubWifistatus == true ? C_ON : C_OFF;
    if (WIFI_IS_CONNECT_FLAG == C_ON) {
      WIFI_LAST_STATUS_FLAG = C_ON;
      WIFI_GET_EXTERNAL_IP = C_ON;
    }
    g_ubPreWifistatus = g_ubWifistatus;
  }
  return g_ubWifistatus;
#endif
}

void App_ModuleNetworkReconnect(void) {
#ifdef MITA_DEV
  WiFi.disconnect();
  WiFi.begin(WIFI_SSID, WIFI_PASS);
#else
  DEBUG(F("LAN Reconnect"));
  digitalWrite(ETH_CONNECT_PIN, LOW);
#endif
}

void App_GetExternalIp(void) {
  HTTPClient http;

  http.begin("https://api.ipify.org/?format=text");
  int httpCode = http.GET();

  if (httpCode == 200) {
    deviceIp = http.getString();
    DEBUG(deviceIp);
  } else {
    DEBUG(F("Error on HTTP request"));
  }

  http.end();
}

/*--- For Thingsboard ---*/
void App_ThingsboardConnect(void) {
  static bool subscribed = false;

  DEBUG(F("Connect Thingsboard Server..."));
  if (!tb.connect(THINGSBOARD_SERVER, THINGSBOARD_TOKEN, THINGSBOARD_PORT)) {
    DEBUG("Failed to connect");
    return;
  }

  // if (subscribed == false) {
  if (tb.RPC_Subscribe(callbacks.cbegin(), callbacks.cend())) {
    subscribed = true;
  } else {
    subscribed = false;
    DEBUG(F("Subscribe fail"));
  }

  if (tb.Shared_Attributes_Request(sharedAttributesRequestCallback)) {
    DEBUG(F("Failed to request shared attributes"));
  }

  if (tb.Shared_Attributes_Subscribe(sharedAttributesSubscirbeCallback)) {
    DEBUG(F("Failed to subcribe shared attributes"));
  }

  App_ThingsboardCheckConnect();

  TB_SYNC_FLAG = C_ON;
}

void App_ThingsboardCheckConnect(void) {
  g_ubThingsboardStatus = tb.connected();

  if (!App_CheckModuleNetwork()) {
    DEBUG(F("network error"));
    g_ubThingsboardStatus = false;
  }

  if (g_ubPreThingsboardStatus != g_ubThingsboardStatus) {
    if (g_ubThingsboardStatus == false) {
      DEBUG("Thingsboard: Not Connected");
      TB_IS_CONNECT_FLAG = C_OFF;
      g_tLedStatus = TB_DISCONNECTED_STAGE;
      g_ulNetworkLastTimeDisconnect = millis();

    } else if (g_ubThingsboardStatus == true) {
      DEBUG("Thingsboard: Connected");
      TB_IS_CONNECT_FLAG = C_ON;
      TB_CONNECT_DONE_FLAG = C_ON;
      g_tLedStatus = TB_CONNECTED_STAGE;
    }

    g_ubPreThingsboardStatus = g_ubThingsboardStatus;
  }
}

void App_ThingsboardSync(void) {
  tb.sendAttributeInt("outlet1", g_ubPower1State);
  tb.sendAttributeInt("outlet2", g_ubPower2State);
}

/*--- For ethernet ---*/
void App_EthernetInit(void) {
  DEBUG("Waiting ethernet init...");
  pinMode(14, OUTPUT);
  pinMode(12, OUTPUT);

  pinMode(RESET_PIN, OUTPUT);

  digitalWrite(RESET_PIN, LOW);

#ifndef MITA_DEV
  WT32_ETH01_onEvent();

  ETH.begin(ETH_ADDR, ETH_POWER_PIN, ETH_MDC_PIN, ETH_MDIO_PIN, ETH_TYPE, ETH_CLK_MODE);

  //ETH.config(myIP, myGW, mySN, myDNS);

  Serial.println(F("Waiting for connect..."));
  WT32_ETH01_waitForConnect();
#endif

  Serial.println(F("Lan connected"));
}

/*--- For Wifi ---*/
bool App_WifiStationInit(void) {
#ifdef MITA_DEV
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  WiFi.begin(WIFI_SSID, WIFI_PASS);
  DEBUG("Connecting to WiFi");

  uint32_t t = millis();
  while (WiFi.status() != WL_CONNECTED && millis() - t < 8000L) {
    delay(100);
  }
#endif

  return true;
}

void App_WifiAPInit(void) {
  g_tLedStatus = AP_MODE_STAGE;
  // Connect to Wi-Fi network with SSID and password
  DEBUG("Setting AP (Access Point)");
  // NULL sets an open Access Point
  WiFi.softAP("Stratus-Device", NULL);
  IPAddress IP = WiFi.softAPIP();
  DEBUG("AP IP address: ");
  DEBUG(IP);

  // Web Server Root URL
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(SPIFFS, "/wifimanager.html", "text/html");
  });

  server.serveStatic("/", SPIFFS, "/");

  server.on("/", HTTP_POST, [](AsyncWebServerRequest *request) {
    int params = request->params();
    for (int i = 0; i < params; i++) {
      AsyncWebParameter *p = request->getParam(i);
      if (p->isPost()) {
        // HTTP POST ssid value
        if (p->name() == PARAM_INPUT_1) {
          ssid = p->value().c_str();
          DEBUG("SSID set to: ");
          DEBUG(ssid);
          // Write file to save value
          writeFile(SPIFFS, ssidPath, ssid.c_str());
        }

        // HTTP POST pass value
        if (p->name() == PARAM_INPUT_2) {
          pass = p->value().c_str();
          DEBUG("Password set to: ");
          DEBUG(pass);
          // Write file to save value
          writeFile(SPIFFS, passPath, pass.c_str());
        }

        // HTTP POST ip value
        if (p->name() == PARAM_INPUT_3) {
          ip = p->value().c_str();
          DEBUG("IP Address set to: ");
          DEBUG(ip);
          // Write file to save value
          writeFile(SPIFFS, ipPath, ip.c_str());
        }

        // HTTP POST gateway value
        if (p->name() == PARAM_INPUT_4) {
          gateway = p->value().c_str();
          DEBUG("Gateway set to: ");
          DEBUG(gateway);
          // Write file to save value
          writeFile(SPIFFS, gatewayPath, gateway.c_str());
        }
      }
    }

    request->send(200, "text/plain", "Done. ESP will restart and connect to your router");
    delay(3000);
    ESP.restart();
  });
  server.begin();
}
