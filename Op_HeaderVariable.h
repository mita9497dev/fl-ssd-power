#ifndef _OP_HEADER_VARIABLE_H_
#define _OP_HEADER_VARIABLE_H_
 
#ifdef GLOBAL_DEFINE
#define HEADER_VARIABLE
#else
#define HEADER_VARIABLE extern
#endif
 
#include "main.h"
typedef enum
{
  IDLE_STAGE,
  AP_MODE_STAGE,
  CONNECTING_STAGE,
  CONNECTED_STAGE,
  DISCONNECT_STAGE,
  TB_DISCONNECTED_STAGE,
  TB_CONNECTED_STAGE
} LedStatus_t;
/*--- Variable for wifi ---*/
HEADER_VARIABLE String ssid;
HEADER_VARIABLE String pass;
HEADER_VARIABLE String ip;
HEADER_VARIABLE String gateway;
HEADER_VARIABLE String BlynkToken;
HEADER_VARIABLE String deviceIp;
HEADER_VARIABLE uint16_t NetworkTimeoutReset;

HEADER_VARIABLE double g_dUpTime;
HEADER_VARIABLE uint32_t g_ulNetworkLastTimeDisconnect;
HEADER_VARIABLE uint32_t g_ulNetworkLastTimeReconnect;
HEADER_VARIABLE uint8_t g_ubWifistatus;
HEADER_VARIABLE uint8_t g_ubPreWifistatus;
HEADER_VARIABLE bool g_ubThingsboardStatus;
HEADER_VARIABLE bool g_ubPreThingsboardStatus;
/*--- Variable for Key ---*/
HEADER_VARIABLE uint8_t g_ubKeyOffCount;
HEADER_VARIABLE uint32_t g_ulKeyInValue;
HEADER_VARIABLE uint8_t g_ubKeyPositionValue;
HEADER_VARIABLE uint16_t g_uwKeyContinousCount;
HEADER_VARIABLE uint8_t g_ubUpDownMoveValue;
/*--- Variable for output ---*/
HEADER_VARIABLE bool g_ubPower1State;
HEADER_VARIABLE bool g_ubPower2State;
HEADER_VARIABLE uint32_t g_ubPower1RebootTime;
HEADER_VARIABLE uint32_t g_ubPower2RebootTime;
HEADER_VARIABLE float g_ufPacketsLoss;
HEADER_VARIABLE float g_ufLatency;
/*--- Variable for time ---*/
HEADER_VARIABLE uint16_t g_aubTime[6];
HEADER_VARIABLE uint16_t g_aubAlarm1OnTime[6];
HEADER_VARIABLE uint16_t g_aubAlarm1OffTime[6];
HEADER_VARIABLE uint16_t g_aubAlarm2OnTime[6];
HEADER_VARIABLE uint16_t g_aubAlarm2OffTime[6];
HEADER_VARIABLE bool ALARM_POWE_1_ENABLE_FLAG;
HEADER_VARIABLE bool ALARM_POWE_2_ENABLE_FLAG;
HEADER_VARIABLE uint16_t g_ulSetTimeOff1;
/*--- Variable for LED ---*/
HEADER_VARIABLE LedStatus_t g_tLedStatus;

#endif // _OP_HEADER_VARIABLE_H_
