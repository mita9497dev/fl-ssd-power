#ifndef _MID_CONTROL_POWER_H_
#define _MID_CONTROL_POWER_H_
 
#include "main.h"
 
void Mid_ControlPowerInit(void);
void Mid_ControlPowerHandle(void);
void Mid_Power1Reboot(void);
void Mid_syncPower1(void);
void Mid_syncPower2(void);
 
#endif // _MID_CONTROL_POWER_H_
