#ifndef _MID_FILE_OPERATION_H_
#define _MID_FILE_OPERATION_H_
 
#include "main.h"
#include "SPIFFS.h"
 
void initSPIFFS();
String readFile(fs::FS &fs, const char * path);
void writeFile(fs::FS &fs, const char * path, const char * message);
 
#endif // _MID_FILE_OPERATION_H_
