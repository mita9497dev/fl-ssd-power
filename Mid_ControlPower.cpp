#include "Mid_ControlPower.h"
 
void Mid_ControlPowerInit(void)
{
  pinMode(POWER_1, OUTPUT);
  pinMode(POWER_2, OUTPUT);
  g_ubPower1State = LOW;
  g_ubPower2State = LOW;
}

void Mid_Power1Reboot(void) 
{
    if (POWER_1_REBOOT_FLAG == C_ON) return;

    g_ubPower1RebootTime = millis();
    POWER_1_REBOOT_FLAG = C_ON;
    g_ubPower1State = 0;
    digitalWrite(POWER_1, g_ubPower1State);
    DEBUG("Power 1: Low");
}

void Mid_ControlPowerHandle(void)
{
    uint32_t t = millis();

    if (POWER_1_REBOOT_FLAG == C_ON) 
    {
        if (g_ubPower1State == 1) 
        {
            DEBUG("Power 1: Low");
            g_ubPower1RebootTime = millis();
            g_ubPower1State = 0;
            digitalWrite(POWER_1, g_ubPower1State);
            Mid_syncPower1();
        }
        else if (t - g_ubPower1RebootTime >= 4000L) 
        {
            DEBUG("Power 1: high");
            POWER_1_REBOOT_FLAG = C_OFF;
            g_ubPower1State = 1;
            digitalWrite(POWER_1, g_ubPower1State);
            Mid_syncPower1();
        }
    }

    if (POWER_2_REBOOT_FLAG == C_ON) 
    {
        if (g_ubPower2State == 1) 
        {
            DEBUG("Power 2: Low");
            g_ubPower2RebootTime = millis();
            g_ubPower2State = 0;
            digitalWrite(POWER_1, g_ubPower2State);
            Mid_syncPower2();
        }
        else if (t - g_ubPower2RebootTime >= 4000L) 
        {
            DEBUG("Power 2: high");
            POWER_2_REBOOT_FLAG = C_OFF;
            g_ubPower2State = 1;
            digitalWrite(POWER_1, g_ubPower2State);
            Mid_syncPower2();
        }
    }
  
    if(POWER_1_CHANGE_FLAG == C_ON)
    {
        digitalWrite(POWER_1, g_ubPower1State);
        digitalWrite(LED_OUTPUT_1, g_ubPower1State);
        DEBUG((g_ubPower1State == HIGH) ? "Power 1: high" : "Power 1: low");
        TB_SYNC_FLAG = C_ON;
        Mid_syncPower1();
        
        POWER_1_CHANGE_FLAG = C_OFF;
    }
  
    if(POWER_2_CHANGE_FLAG == C_ON)
    {
        digitalWrite(POWER_2, g_ubPower2State);
        digitalWrite(LED_OUTPUT_2, g_ubPower2State);
        DEBUG((g_ubPower2State == HIGH)? "Power 2: high" : "Power 2: low");
        TB_SYNC_FLAG = C_ON;
        Mid_syncPower2();
    
        POWER_2_CHANGE_FLAG = C_OFF;
    }
}
