#include "Mid_LedControl.h"
 
#define TIME_100MS_BY_20MS  (5)
#define TIME_500MS_BY_20MS  (25)
#define TIME_1S_BY_20MS     (50)
#define TIME_2S_BY_20MS     (100)
 
void Mid_LedInit(void)
{
  pinMode(LED_OUTPUT_1, OUTPUT);
  pinMode(LED_OUTPUT_2, OUTPUT);
  pinMode(LED_STATUS_1, OUTPUT);
  pinMode(LED_STATUS_2, OUTPUT);
}
 
void Mid_LedAllOn(void)
{
  digitalWrite(LED_OUTPUT_1, HIGH);
  digitalWrite(LED_OUTPUT_2, HIGH);
  digitalWrite(LED_STATUS_1, HIGH);
  digitalWrite(LED_STATUS_2, HIGH);
}
 
void Mid_LedAllOff(void)
{
  digitalWrite(LED_OUTPUT_1, LOW);
  digitalWrite(LED_OUTPUT_2, LOW);
  digitalWrite(LED_STATUS_1, LOW);
  digitalWrite(LED_STATUS_2, LOW);
}
 
void Mid_LedHanler(void)
{
  static uint16_t ulLedCountTime = 0;
  static bool uBLedStatus = LOW;
  switch (g_tLedStatus)
  {
    case IDLE_STAGE:
      break;
    case AP_MODE_STAGE:
      if(ulLedCountTime >= TIME_500MS_BY_20MS)
      {
        digitalWrite(LED_STATUS_1, uBLedStatus);
        uBLedStatus = !uBLedStatus;
        digitalWrite(LED_STATUS_2, uBLedStatus);
        ulLedCountTime = 0;
      }
      break;
    case CONNECTING_STAGE:
      if(ulLedCountTime >= TIME_100MS_BY_20MS)
      {
        digitalWrite(LED_STATUS_1, uBLedStatus);
        uBLedStatus = !uBLedStatus;
        ulLedCountTime = 0;
      }
      break;
    case CONNECTED_STAGE:
      if(ulLedCountTime >= TIME_2S_BY_20MS)
      {
        digitalWrite(LED_STATUS_1, uBLedStatus);
        uBLedStatus = !uBLedStatus;
        ulLedCountTime = 0;
      }
      break;
    case DISCONNECT_STAGE:
      break;
    case TB_DISCONNECTED_STAGE:
      digitalWrite(LED_STATUS_1, HIGH);
      digitalWrite(LED_STATUS_2, LOW);
      break;
    case TB_CONNECTED_STAGE:
      digitalWrite(LED_STATUS_2, HIGH);
      digitalWrite(LED_STATUS_1, LOW);
      break;
    default:
      break;
  }
  ulLedCountTime++;
}
