#ifndef _OP_HEADER_FLAG_H_
#define _OP_HEADER_FLAG_H_
 
#ifdef GLOBAL_DEFINE
#define HEADER_VARIABLE
#else
#define HEADER_VARIABLE extern
#endif
 
typedef struct
{
  uint8_t b0 : 1;   /// 0 bit memoery define
  uint8_t b1 : 1;   /// 1 bit memoery define
  uint8_t b2 : 1;   /// 2 bit memoery define
  uint8_t b3 : 1;   /// 3 bit memoery define
  uint8_t b4 : 1;   /// 4 bit memoery define
  uint8_t b5 : 1;   /// 5 bit memoery define  
  uint8_t b6 : 1;   /// 6 bit memoery define
  uint8_t b7 : 1;   /// 7 bit memoery define
} USER_BYTE_FIELD;
 
typedef union 
{
  uint8_t   byte;
  USER_BYTE_FIELD   byte_bit; 
} USER_TBYTE;
 
/*--- Flag for Thingsboard ---*/
HEADER_VARIABLE USER_TBYTE ThingsboardFlag1;
#define TB_1S_CHECK_CONNECT_FLAG        ThingsboardFlag1.byte_bit.b0
#define TB_IS_CONNECT_FLAG              ThingsboardFlag1.byte_bit.b1
#define TB_SYNC_FLAG                    ThingsboardFlag1.byte_bit.b2
#define TB_CONNECT_DONE_FLAG            ThingsboardFlag1.byte_bit.b3
#define RUN_1_TIME_ENABLE_FLAG          ThingsboardFlag1.byte_bit.b4
#define TB_INIT_1_TIME_FLAG             ThingsboardFlag1.byte_bit.b4

/*--- Flag for wifi ---*/
HEADER_VARIABLE USER_TBYTE WifiFlag1;
#define WIFI_1S_CHECK_CONNECT_FLAG      WifiFlag1.byte_bit.b0
#define WIFI_IS_CONNECT_FLAG            WifiFlag1.byte_bit.b1
#define WIFI_ENTER_AP_MODE              WifiFlag1.byte_bit.b2
#define WIFI_ENTER_ST_MODE              WifiFlag1.byte_bit.b3
#define WIFI_LAST_STATUS_FLAG           WifiFlag1.byte_bit.b4
#define WIFI_GET_EXTERNAL_IP            WifiFlag1.byte_bit.b5

/*--- Flag for Key ---*/
HEADER_VARIABLE USER_TBYTE KeyFlag1;
#define KEY_1CLICK_FLAG                 KeyFlag1.byte_bit.b0
#define KEY_0_5SEC_CLICK_FLAG           KeyFlag1.byte_bit.b1
#define KEY_1SEC_CLICK_FLAG             KeyFlag1.byte_bit.b2
#define KEY_1_5SEC_CLICK_FLAG           KeyFlag1.byte_bit.b3
#define KEY_2SEC_CLICK_FLAG             KeyFlag1.byte_bit.b4
#define KEY_3SEC_CLICK_FLAG             KeyFlag1.byte_bit.b5
#define KEY_5SEC_CLICK_FLAG             KeyFlag1.byte_bit.b6
#define KEY_7SEC_CLICK_FLAG             KeyFlag1.byte_bit.b7
 
HEADER_VARIABLE USER_TBYTE KeyFlag2;
#define KEY_RELEASE_CLICK_FLAG          KeyFlag2.byte_bit.b0
#define ERROR_KEYSHORT_FLAG             KeyFlag2.byte_bit.b1

/*--- Flag for control power ---*/
HEADER_VARIABLE USER_TBYTE ControlPowerFlag1;
#define POWER_1_CHANGE_FLAG            ControlPowerFlag1.byte_bit.b0
#define POWER_2_CHANGE_FLAG            ControlPowerFlag1.byte_bit.b1
#define POWER_1_RESTART_HIGH_FLAG      ControlPowerFlag1.byte_bit.b2
#define POWER_1_RESTART_DONE_FLAG      ControlPowerFlag1.byte_bit.b3
#define POWER_1_REBOOT_FLAG            ControlPowerFlag1.byte_bit.b4
#define POWER_2_REBOOT_FLAG            ControlPowerFlag1.byte_bit.b5

/*--- Flag for time ---*/
HEADER_VARIABLE USER_TBYTE TimeFlag1;
#define TIME_1S_COUNT_FLAG            TimeFlag1.byte_bit.b0
#define TIME_OFF_1_ENABLE_FLAG        TimeFlag1.byte_bit.b1

/*--- Flag for ping --*/
HEADER_VARIABLE USER_TBYTE PingFlag1;
#define PING_PROCESS_FLAG            PingFlag1.byte_bit.b0
#define PING_SEND_DATA_FLAG          PingFlag1.byte_bit.b1
#define STATE_SYNC_FLAG              PingFlag1.byte_bit.b2

#endif // _OP_HEADER_FLAG_H_
