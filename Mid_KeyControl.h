#ifndef _MID_KEY_H_
#define _MID_KEY_H_
 
#include "main.h"
 
void Mid_KeyInit(void);
void Mid_KeyHandler(void);
 
#endif // _MID_KEY_H_
