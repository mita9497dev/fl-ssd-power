#include "App_Control.h"
 
void App_Controlhandle(void)
{
  if(KEY_1CLICK_FLAG == C_ON)
  {
    if(g_ulKeyInValue == 0x00000002)
    {
      g_ubPower1State = !g_ubPower1State;
      POWER_1_CHANGE_FLAG = C_ON;
    }
 
    if(g_ulKeyInValue == 0x00000004)
    {
      g_ubPower2State = !g_ubPower2State;
      POWER_2_CHANGE_FLAG = C_ON;
    }
  }
  
  if(KEY_3SEC_CLICK_FLAG == C_ON)
  {
    if(g_ulKeyInValue == 0x00000001)
    {
      //DEBUG("WIFI: Enter AP mode");
      //WIFI_ENTER_AP_MODE = C_ON;
    }
  }
  
  KEY_1CLICK_FLAG = C_OFF;
  KEY_0_5SEC_CLICK_FLAG = C_OFF;
  KEY_1SEC_CLICK_FLAG = C_OFF;
  KEY_1_5SEC_CLICK_FLAG = C_OFF;
  KEY_2SEC_CLICK_FLAG = C_OFF;
  KEY_3SEC_CLICK_FLAG = C_OFF;
  KEY_5SEC_CLICK_FLAG = C_OFF;
  KEY_7SEC_CLICK_FLAG = C_OFF;
  KEY_RELEASE_CLICK_FLAG = C_OFF;
}
